var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
const client = require('mongodb').MongoClient;
const fileUpload = require('express-fileupload');
var api = require('./api.js');

app.use(fileUpload());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var url = 'mongodb://localhost:5000/sparkData';
var db = null;

client.connect(url, (err, database) => {
    if (!err) {
        db = database;
    }
});

app.get('/api/', (req, res) => {
    res.json({
        msg: 'working'
    });
});

app.get('/api/users', (req, res) => {
    db.collection('users')
        .find({})
        .toArray((err, users) => {
            if (!err) {
                res.json({
                    msg: 'users loaded',
                    users: users
                })
            } else {
                res.json({
                    msg: 'load failed'
                });
            }
        });
});

app.get('/api/posts', (req, res) => {
    db.collection('posts')
        .find({})
        .toArray((err, posts) => {
            if (!err) {
                res.json({
                    msg: 'posts loaded',
                    posts: posts
                })
            } else {
                res.json({
                    msg: 'load failed'
                });
            }
        });
});

app.post('/api/crearUsuario', (req, res) => {
    db.collection('users')
        .find({
            email: req.body.email
        }).toArray((err, users) => {
            if (!err) {
                var newUser = {
                    name: req.body.name,
                    email: req.body.email,
                    country: req.body.country,
                    password: req.body.password
                };

                db.collection('users').insert(newUser, (errInsert) => {
                    if (!errInsert) {
                        res.json({
                            msg: 'register',
                            email: req.body.email
                        });
                    } else {
                        res.json({
                            msg: 'Error la insertar usuario'
                        })
                    }
                });
            } else {
                res.json({
                    msg: 'El usuario ya existe'
                });
            }
        });
});

app.post('/api/login', (req, res) => {
    db.collection('users')
        .find({
            email: req.body.email,
            pass: req.body.pass
        })
        .toArray((err, users) => {
            if (!err && users.length > 0) {
                res.json({
                    msg: 'logged',
                    user: users[0]
                });
            } else {
                res.json({
                    msg: 'Usuario o contraseña incorrecto'
                });
            }
        });
});

app.post('/api/crearPost', (req, res) => {
        db.collection('posts')
            .find({})
            .toArray((err, posted) => {
                if (!err) {
                    var newPost = {
                        name: req.body.name,
                        country: req.body.country,
                        content: req.body.content,
                        likes: req.body.likes,
                        image: req.body.image,
                        comments: req.body.comments,
                        profilePic: req.body.profilePic,
                        ident: req.body.ident
                    };

                    db.collection('posts').insert(newPost, (errInsert) => {
                        if (!errInsert) {
                            res.json({
                                msg: 'posted',
                                post: newPost
                            });
                        } else {
                            res.json({
                                msg: 'Erroral postear'
                            })
                        }
                    });
                } else {
                    res.json({
                        msg: 'Error al postear'
                    });
                }
            });
});

app.post('/api/crearPostImg', (req, res) => {
    if (!req.files) {
        return res.json({
            msg: 'sin archivo'
        });
    }

    var picture = req.files.image;

    picture.mv(path.join(__dirname, `public/postImg/${picture.name}`), (err) => {
        if (!err) {
            db.collection('posts')
                .find({})
                .toArray((err, posted) => {
                    if (!err) {
                        var newPost = {
                            name: req.body.name,
                            country: req.body.country,
                            content: req.body.content,
                            likes: req.body.likes,
                            image: picture.name,
                            comments: req.body.comments,
                            profilePic: req.body.profilePic,
                            ident: req.body.ident
                        };

                        db.collection('posts').insert(newPost, (errInsert) => {
                            if (!errInsert) {
                                res.json({
                                    msg: 'posted',
                                    post: newPost
                                });
                            } else {
                                res.json({
                                    msg: 'Error al postear'
                                })
                            }
                        });
                    } else {
                        res.json({
                            msg: 'Error al postear'
                        });
                    }
                });
        }
    });
});

app.post('/api/likePost', (req,res) => {
    db.collection('posts')
    .updateOne({
        ident: req.body.ident
    }, {
        $set: {
            likes: req.body.allLikes
        }
    });
});

app.post('/api/commentPost', (req,res) => {
    db.collection('posts')
    .updateOne({
        ident: req.body.ident
    }, {
        $push: {
            comments: {
                profilePic: req.body.profilePic,
                name: req.body.name,
                comment: req.body.content
            }
        }
    });
});

app.post('/api/subirImg/:email', (req, res) => {
    if (!req.files) {
        return res.json({
            msg: 'sin archivo'
        });
    }

    console.log(req.params.email, req.body.texto);
    var foto = req.files.foto;

    foto.mv(path.join(__dirname, `public/postImg/${foto.name}`), function (err) {
        if (!err) {
            res.json({
                msg: 'ok'
            });

            db.collection('usuarios')
                .updateOne({
                    email: req.params.email
                }, {
                    $push: {
                        foto: foto.name,
                        texto: req.body.texto
                    }
                });

        } else {
            res.json({
                mensaje: 'error',
                error: err
            });
        }
    });
});

app.use('/js', express.static('public/js'));
app.use('/img', express.static('public/img'));
app.use('/postImg', express.static('public/postImg'));
app.use('/profilePicture', express.static('public/profilePicture'));
app.use('/style', express.static('public/style'));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '/public/index.html'));
});

app.listen(5000);
