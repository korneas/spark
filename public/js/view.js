var view = {

    user: {
        email: "",
        pass: "",
        name: "",
        country: "",
        pic: "../profilePicture/korneas.jpg"
    },

    posted: null,

    createInit: function createInit() {
        var div = document.createElement('div');

        div.innerHTML = `
        <div class="slideInit">
           <div class="slides">
               <div class="slide-1 slide"><p class="titleSlide">Guarda tus recuerdos <br></p><p class="descriptionSlide">Para mantenerlos a salvo y no de perderlos</p></div>
               <div class="slide-2 slide"><p class="titleSlide">Marca ubicaciones<br></p><p class="descriptionSlide">Y obten el recorrido que has hecho</p></div>
               <div class="slide-3 slide"><p class="titleSlide">Agrega amigos<br></p><p class="descriptionSlide">Y comenta sus estados y publicaciones</p></div>
               <div class="slide-4 slide"><p class="titleSlide">Viaja y conoce<br></p><p class="descriptionSlide">Todos los rincones del mundo</p></div>
           </div>
       </div>
        <div class="logFrag">
            <div class="log">
                <div class="logoLog">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 810 483" class="logo" xml:space="preserve">
    <style type="text/css">
	.st1{fill:#FC356E;}
	.st2{fill:#FFA9CA;}
	.st3{fill:#CB5DD1;}
	.st4{fill:url(#SVGID_1_);}
	.st5{fill:url(#SVGID_2_);}
	.st6{fill:url(#SVGID_3_);}
	.st7{fill:url(#SVGID_4_);}
    </style>
    <g id="Layer_3">
		<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="405.1914" y1="-3" x2="405.1914" y2="489.0041">
			<stop  offset="1.982887e-07" style="stop-color:#FC356E"/>
			<stop  offset="1" style="stop-color:#CB5DD1"/>
		</linearGradient>
		<path class="st4" d="M810.2,226c-0.8-34-32-52-64-17c-7.2,7.8-2.7,45.1,5,34c27-39,37-33,37-18c0,32-50,95-73,95
			c-32.5,0,11-108-22-147c0,0,26-3,43-24s11-55-11-55c-16.1,0-49.2,60.8-65.9,93.6c17.7-51.8,40.2-118.9,47.4-144.1
			c6.8-23.6-11.3-37.3-20.9-42.7c-1.9-1.1-4,0.8-3.2,2.8c4,10.4,3.5,15.7-0.4,32.4c-2.4,10.2-25.2,73.3-43.9,125.9
			c-12.8,27.5-32.9,66.1-45.1,66.1c-18,0,42-89,13-89c-9.4,0-20.2,1.8-30,3.1c1.3-5.5,2-9.7,2-12.1c0-18-17-27-30-27s-28,14-28,30
			c0,11.8,12,23.3,36.4,24.3c-11.9,31.9-34.7,77.7-47.4,77.7c-11,0,3.7-25.6,8-35c5-11-14-22-21-7c0,0-31,55-51,55
			c-16,0,28-76,47-76c7,0,9.4,3.1,14.5,4.1c2.8,0.6,4.6-2.8,2.7-4.8c-7-7.4-17.2-15.3-28.2-15.3c-32,0-67,45-67,76
			c0,2.6,0.1,5.2,0.3,7.6c-6.5,7.9-14.2,17.4-21.3,17.4c-14,0,41-79-6-79c-28.8,0-65.6,79.3-86.8,131.7
			c13-42.5,28.4-86.4,41.8-108.7c14-20-22-34-27-23c0,0-0.2,0.3-0.4,1c0-0.3-0.1-0.5-0.1-0.5s-26,61-57,61S309.2,127,309.2,78
			s-52.5-61.5-86.5-61.5S120.2,55,120.2,135s60,152,60,240S44.7,487.5,44.7,392.5c0-65.8,30.2-90.4,48.8-99.2c4-1.9,3.3-7.9-1.1-8.7
			c-31.9-6.2-92.2-9.8-92.2,92.4c0,70.5,60,106.5,125.5,106.5c71.5,0,99-51,99-116S166.2,211,166.2,156s50-105,90-105
			s-44.5,93-44.5,160.5c0,38,30,62,70,38c2.1-1.3,4.1-2.6,5.9-4c-23.4,60.8-51.3,143.9-38.4,165.5c5,6,21,14,22,6
			c0.1-0.6,0.4-2,0.8-4c0.1-0.2,56.2-146,80.2-182c24-36,31-44,11,3c-12.1,28.5-9,55,8,55c16,0,36.3-16.7,48-32.9
			c3.9,6.8,10.6,10.9,21,10.9c12.1,0,25.1-9.5,36.6-21.4c0.3,10,5.5,17.4,20.4,17.4c32.7,0,62.5-66.6,74.9-106.4
			c4.7-0.5,9.7-1.4,15.1-2.6c13-3-59,92,4,92c8.7,0,16.7-6.5,23.9-16.1c-0.4,1.2-0.6,2.2-0.9,3.1c-3.5,13.5,21,20,26,10
			c0,0,16.7-33.7,35.7-67.7c2.4,10.6,6.2,26.9,3.3,50.7c-6,50-23,111,35,111C765.2,337,811.3,272.4,810.2,226z M546.2,133
			c0-19,17-15,18-4c0.2,2.5-0.9,7.7-2.9,14.3C552.3,143.3,546.2,141,546.2,133z M725.2,106c12.5,0-21.4,49.2-39.9,52.8
			C701.7,130.5,718,106,725.2,106z"/>
		<linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="498.2494" y1="-3" x2="498.2494" y2="489.0041">
			<stop  offset="1.982887e-07" style="stop-color:#FC356E"/>
			<stop  offset="1" style="stop-color:#CB5DD1"/>
		</linearGradient>
		<path class="st5" d="M649.8,294.9c-32.6-12-54-22.3-122.3-18.9c-99.2,4.9-141.6,34.4-185.5,60.4c-17.6,10.4,2,40.4,32.2,24.6
			c37.1-19.4,78.1-39.6,143.7-42.9c65.7-3.3,88.8-3.7,129.4,6.7C663.1,328.8,666.9,301.2,649.8,294.9z"/>
	</g>
    </svg>
    </div>
                <form action="" id="loginForm" class="form login">
                    <div class="form_field">
                        <label for="login_username"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" class="icon" xml:space="preserve"><g>
		<path d="M256,0C115.03,0,0,115.05,0,256c0,142.255,116.376,256,256,256c67.985,0,132.324-26.698,180.812-75.188    C485.298,388.327,512,324.113,512,256C512,115.03,396.95,0,256,0z M391,437.141c-9.456,7.065-19.488,13.396-30,18.935V407    c0-8.284-6.716-15-15-15s-15,6.716-15,15v62.191C307.519,477.476,282.28,482,256,482c-26.28,0-51.519-4.524-75-12.809V407    c0-8.284-6.716-15-15-15s-15,6.716-15,15v49.076c-10.512-5.539-20.544-11.87-30-18.935V407c0-41.907,33.645-76,75-76    c2.699,0,116.757,0,120,0c41.355,0,75,34.093,75,76V437.141z M181,226c0-41.355,33.645-75,75-75    c40.522,0,73.642,32.304,74.959,72.511c-1.28,40.924-32.594,75.376-70.904,77.382C216.784,303.156,181,268.881,181,226z     M421,410.262c0-61.902-44.063-103.28-93.742-108.651c1.956-1.917,3.855-3.919,5.688-6.008    C350.499,275.597,361,249.268,361,220.907c0-1.051-0.108-2.077-0.314-3.067C356.508,163.741,311.151,121,256,121    c-57.897,0-105,47.103-105,105c0,28.899,11.477,55.784,32.323,75.775C134.415,307.744,91,348.728,91,410.262    C53.188,369.844,30,315.583,30,256C30,131.383,131.383,30,256,30s226,101.383,226,226C482,315.583,458.812,369.844,421,410.262z"/>
	</g></svg><span class="hidden">Username</span></label>
                        <input id="logUsername" type="text" name="username" class="form__input" placeholder="Correo" required>
                    </div>

                    <div class="form_field">
                        <label for="login__password"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 512 512" class="icon" xml:space="preserve"><g><path d="M298.885,372.957C309.746,361.839,316,346.795,316,331c0-33.084-26.916-60-60-60c-33.084,0-60,26.916-60,60
			c0,15.795,6.254,30.839,17.115,41.957l-16.575,60.052c-1.246,4.514-0.312,9.352,2.525,13.078S206.317,452,211,452h90
			c4.683,0,9.098-2.187,11.935-5.913c2.837-3.726,3.771-8.563,2.525-13.078L298.885,372.957z M267.442,371.793L281.299,422H230.7
			l13.857-50.207c1.684-6.101-0.643-12.603-5.816-16.25C230.764,349.918,226,340.743,226,331c0-16.542,13.458-30,30-30
			s30,13.458,30,30c0,9.743-4.764,18.918-12.742,24.543C268.085,359.191,265.758,365.692,267.442,371.793z"/></g><g><path d="M406,211v-61C406,67.29,338.71,0,256,0S106,67.29,106,150v61c-24.813,0-45,20.187-45,45v211c0,24.813,20.187,45,45,45h300
			c24.813,0,45-20.187,45-45V256C451,231.187,430.813,211,406,211z M136,150c0-66.168,53.832-120,120-120s120,53.832,120,120v61h-30
			v-61c0-49.626-40.374-90-90-90c-49.626,0-90,40.374-90,90v61h-30V150z M316,150v61H196v-61c0-33.084,26.916-60,60-60
			S316,116.916,316,150z M421,467c0,8.271-6.729,15-15,15H106c-8.271,0-15-6.729-15-15V256c0-8.271,6.729-15,15-15h300
			c8.271,0,15,6.729,15,15V467z"/></g></svg><span class="hidden">Password</span></label>
                        <input id="logPassword" type="password" name="password" autocomplete="off" class="form__input" formenctype="application/x-www-form-urlencoded" placeholder="Contraseña" required>
                    </div>

                    <div class="form_field">
                        <input type="submit" value="Ingresar">
                    </div>

                    <p>No tienes cuenta? <a id="goReg" href="#"><b>Registrarse</b></a></p>

                </form>
            </div>
        </div>
<div class="regFrag">
            <div class="reg">
                <div class="logoLog">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 810 483" class="logo" xml:space="preserve">
    <style type="text/css">
	.st1{fill:#FC356E;}
	.st2{fill:#FFA9CA;}
	.st3{fill:#CB5DD1;}
	.st4{fill:url(#SVGID_1_);}
	.st5{fill:url(#SVGID_2_);}
	.st6{fill:url(#SVGID_3_);}
	.st7{fill:url(#SVGID_4_);}
    </style>
    <g id="Layer_3">
		<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="405.1914" y1="-3" x2="405.1914" y2="489.0041">
			<stop  offset="1.982887e-07" style="stop-color:#FC356E"/>
			<stop  offset="1" style="stop-color:#CB5DD1"/>
		</linearGradient>
		<path class="st4" d="M810.2,226c-0.8-34-32-52-64-17c-7.2,7.8-2.7,45.1,5,34c27-39,37-33,37-18c0,32-50,95-73,95
			c-32.5,0,11-108-22-147c0,0,26-3,43-24s11-55-11-55c-16.1,0-49.2,60.8-65.9,93.6c17.7-51.8,40.2-118.9,47.4-144.1
			c6.8-23.6-11.3-37.3-20.9-42.7c-1.9-1.1-4,0.8-3.2,2.8c4,10.4,3.5,15.7-0.4,32.4c-2.4,10.2-25.2,73.3-43.9,125.9
			c-12.8,27.5-32.9,66.1-45.1,66.1c-18,0,42-89,13-89c-9.4,0-20.2,1.8-30,3.1c1.3-5.5,2-9.7,2-12.1c0-18-17-27-30-27s-28,14-28,30
			c0,11.8,12,23.3,36.4,24.3c-11.9,31.9-34.7,77.7-47.4,77.7c-11,0,3.7-25.6,8-35c5-11-14-22-21-7c0,0-31,55-51,55
			c-16,0,28-76,47-76c7,0,9.4,3.1,14.5,4.1c2.8,0.6,4.6-2.8,2.7-4.8c-7-7.4-17.2-15.3-28.2-15.3c-32,0-67,45-67,76
			c0,2.6,0.1,5.2,0.3,7.6c-6.5,7.9-14.2,17.4-21.3,17.4c-14,0,41-79-6-79c-28.8,0-65.6,79.3-86.8,131.7
			c13-42.5,28.4-86.4,41.8-108.7c14-20-22-34-27-23c0,0-0.2,0.3-0.4,1c0-0.3-0.1-0.5-0.1-0.5s-26,61-57,61S309.2,127,309.2,78
			s-52.5-61.5-86.5-61.5S120.2,55,120.2,135s60,152,60,240S44.7,487.5,44.7,392.5c0-65.8,30.2-90.4,48.8-99.2c4-1.9,3.3-7.9-1.1-8.7
			c-31.9-6.2-92.2-9.8-92.2,92.4c0,70.5,60,106.5,125.5,106.5c71.5,0,99-51,99-116S166.2,211,166.2,156s50-105,90-105
			s-44.5,93-44.5,160.5c0,38,30,62,70,38c2.1-1.3,4.1-2.6,5.9-4c-23.4,60.8-51.3,143.9-38.4,165.5c5,6,21,14,22,6
			c0.1-0.6,0.4-2,0.8-4c0.1-0.2,56.2-146,80.2-182c24-36,31-44,11,3c-12.1,28.5-9,55,8,55c16,0,36.3-16.7,48-32.9
			c3.9,6.8,10.6,10.9,21,10.9c12.1,0,25.1-9.5,36.6-21.4c0.3,10,5.5,17.4,20.4,17.4c32.7,0,62.5-66.6,74.9-106.4
			c4.7-0.5,9.7-1.4,15.1-2.6c13-3-59,92,4,92c8.7,0,16.7-6.5,23.9-16.1c-0.4,1.2-0.6,2.2-0.9,3.1c-3.5,13.5,21,20,26,10
			c0,0,16.7-33.7,35.7-67.7c2.4,10.6,6.2,26.9,3.3,50.7c-6,50-23,111,35,111C765.2,337,811.3,272.4,810.2,226z M546.2,133
			c0-19,17-15,18-4c0.2,2.5-0.9,7.7-2.9,14.3C552.3,143.3,546.2,141,546.2,133z M725.2,106c12.5,0-21.4,49.2-39.9,52.8
			C701.7,130.5,718,106,725.2,106z"/>
		<linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="498.2494" y1="-3" x2="498.2494" y2="489.0041">
			<stop  offset="1.982887e-07" style="stop-color:#FC356E"/>
			<stop  offset="1" style="stop-color:#CB5DD1"/>
		</linearGradient>
		<path class="st5" d="M649.8,294.9c-32.6-12-54-22.3-122.3-18.9c-99.2,4.9-141.6,34.4-185.5,60.4c-17.6,10.4,2,40.4,32.2,24.6
			c37.1-19.4,78.1-39.6,143.7-42.9c65.7-3.3,88.8-3.7,129.4,6.7C663.1,328.8,666.9,301.2,649.8,294.9z"/>
	</g>
    </svg>
                </div>
                <form action="" id="registerForm" class="form login">
                    <div class="form_field">
                        <label for="register_username"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" class="icon" xml:space="preserve">
<g>
		<path d="M321.53,150.146c14.6-16.328,23.794-38.045,24.435-61.836c0.008-0.271,0.008-0.542,0-0.812    C344.629,38.433,305.111,0,256,0c-49.626,0-90,40.374-90,90c0,22.472,8.18,43.68,23.124,60.231C134.42,153.785,91,199.413,91,255    v60c0,8.284,6.716,15,15,15h300c8.284,0,15-6.716,15-15v-60C421,198.958,376.867,153.031,321.53,150.146z M256,30    c32.605,0,58.867,25.405,59.964,57.91c-1.089,33.132-25.837,60.288-56.539,61.985C221.489,150.988,196,122.084,196,90    C196,56.916,222.916,30,256,30z M391,300h-30v-45c0-8.284-6.716-15-15-15s-15,6.716-15,15v45H181v-45c0-8.284-6.716-15-15-15    s-15,6.716-15,15v45h-30v-45c0-41.355,33.645-75,75-75h120c41.355,0,75,33.645,75,75V300z"/>
    </g>
	<g>
		<path d="M437,362H75c-41.355,0-75,33.645-75,75c0,41.355,33.645,75,75,75h362c41.355,0,75-33.645,75-75    C512,395.645,478.355,362,437,362z M437,482H75c-24.813,0-45-20.187-45-45s20.187-45,45-45h362c24.813,0,45,20.187,45,45    S461.813,482,437,482z"/>
	</g>
	<g>
		<path d="M135,422h-30c-8.284,0-15,6.716-15,15s6.716,15,15,15h30c8.284,0,15-6.716,15-15S143.284,422,135,422z"/>
	</g>
	<g>
		<path d="M226,422h-30c-8.284,0-15,6.716-15,15s6.716,15,15,15h30c8.284,0,15-6.716,15-15S234.284,422,226,422z"/>
	</g>
	<g>
		<path d="M316,422h-30c-8.284,0-15,6.716-15,15s6.716,15,15,15h30c8.284,0,15-6.716,15-15S324.284,422,316,422z"/>
	</g>
	<g>
		<path d="M407,422h-30c-8.284,0-15,6.716-15,15s6.716,15,15,15h30c8.284,0,15-6.716,15-15S415.284,422,407,422z"/>
	</g>
</svg><span class="hidden">Usuario</span></label>
                        <input id="regUsername" type="text" autocomplete="off" name="username" class="form__input" placeholder="Nombre" required>
                    </div>

                    <div class="form_field">
                        <label for="register_email"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" class="icon" xml:space="preserve"><g>
		<path d="M256,0C115.03,0,0,115.05,0,256c0,142.255,116.376,256,256,256c67.985,0,132.324-26.698,180.812-75.188    C485.298,388.327,512,324.113,512,256C512,115.03,396.95,0,256,0z M391,437.141c-9.456,7.065-19.488,13.396-30,18.935V407    c0-8.284-6.716-15-15-15s-15,6.716-15,15v62.191C307.519,477.476,282.28,482,256,482c-26.28,0-51.519-4.524-75-12.809V407    c0-8.284-6.716-15-15-15s-15,6.716-15,15v49.076c-10.512-5.539-20.544-11.87-30-18.935V407c0-41.907,33.645-76,75-76    c2.699,0,116.757,0,120,0c41.355,0,75,34.093,75,76V437.141z M181,226c0-41.355,33.645-75,75-75    c40.522,0,73.642,32.304,74.959,72.511c-1.28,40.924-32.594,75.376-70.904,77.382C216.784,303.156,181,268.881,181,226z     M421,410.262c0-61.902-44.063-103.28-93.742-108.651c1.956-1.917,3.855-3.919,5.688-6.008    C350.499,275.597,361,249.268,361,220.907c0-1.051-0.108-2.077-0.314-3.067C356.508,163.741,311.151,121,256,121    c-57.897,0-105,47.103-105,105c0,28.899,11.477,55.784,32.323,75.775C134.415,307.744,91,348.728,91,410.262    C53.188,369.844,30,315.583,30,256C30,131.383,131.383,30,256,30s226,101.383,226,226C482,315.583,458.812,369.844,421,410.262z"/>
	</g></svg><span class="hidden">Correo</span></label>
                        <input id="regPassword" type="text" name="email" autocomplete="off" class="form__input" formenctype="application/x-www-form-urlencoded" placeholder="Correo" required>
                    </div>

                    <div class="form_field">
                        <label for="register_country"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" class="icon" xml:space="preserve">
	<g>
		<path d="M502.291,150.964l-134.402-50.667C365.42,44.575,319.32,0,263,0c-55.79,0-101.834,43.604-104.831,99.274    c-0.829,15.169,0.233,27.654,7.785,44.771L20.296,89.002c-4.608-1.739-9.776-1.105-13.827,1.696C2.418,93.499,0,98.111,0,103.036    v332c0,6.241,3.865,11.831,9.704,14.034l163.984,61.958c0.001,0,0.002,0.001,0.004,0.001l0.012,0.004    c3.435,1.296,7.286,1.251,10.592,0L338,453.032l153.704,58.002c4.597,1.734,9.766,1.112,13.827-1.696    C509.582,506.537,512,501.925,512,497V165C512,158.757,508.133,153.167,502.291,150.964z M164,475.307L30,424.664V124.729    l134,50.643V475.307z M323,426.628l-129,48.679V192.543c13.498,21.041,31.598,48.421,56.368,87.099    c2.758,4.306,7.519,6.911,12.632,6.911c5.113,0,9.874-2.604,12.632-6.911c19.765-30.862,35.505-54.84,47.368-73.044V426.628z     M337.928,109.619c-0.015,0.213-0.018,0.426-0.023,0.639c-0.984,23.893-11.6,35.494-74.904,133.559    c-69.519-107.746-76.48-113.581-74.877-142.905c0-0.005,0-0.009,0.001-0.014C190.261,61.142,223.15,30,263,30    c41.355,0,75,33.645,75,75C338,106.611,337.979,108.141,337.928,109.619z M482,475.307l-129-48.679V158.523    c5.871-10.567,9.615-19.041,11.893-27.295L482,175.376V475.307z"/>
	</g>
	<g>
		<path d="M263,60c-24.813,0-45,20.187-45,45s20.187,45,45,45s45-20.187,45-45S287.813,60,263,60z M263,120c-8.271,0-15-6.729-15-15    s6.729-15,15-15s15,6.729,15,15S271.271,120,263,120z"/>
	</g>
</svg><span class="hidden">Correo</span></label>
                        <input id="regCountry" type="text" name="country" autocomplete="off" class="form__input" formenctype="application/x-www-form-urlencoded" placeholder="País" required>
                    </div>

                    <div class="form_field">
                        <label for="register_password"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 512 512" class="icon" xml:space="preserve"><g><path d="M298.885,372.957C309.746,361.839,316,346.795,316,331c0-33.084-26.916-60-60-60c-33.084,0-60,26.916-60,60
			c0,15.795,6.254,30.839,17.115,41.957l-16.575,60.052c-1.246,4.514-0.312,9.352,2.525,13.078S206.317,452,211,452h90
			c4.683,0,9.098-2.187,11.935-5.913c2.837-3.726,3.771-8.563,2.525-13.078L298.885,372.957z M267.442,371.793L281.299,422H230.7
			l13.857-50.207c1.684-6.101-0.643-12.603-5.816-16.25C230.764,349.918,226,340.743,226,331c0-16.542,13.458-30,30-30
			s30,13.458,30,30c0,9.743-4.764,18.918-12.742,24.543C268.085,359.191,265.758,365.692,267.442,371.793z"/></g><g><path d="M406,211v-61C406,67.29,338.71,0,256,0S106,67.29,106,150v61c-24.813,0-45,20.187-45,45v211c0,24.813,20.187,45,45,45h300
			c24.813,0,45-20.187,45-45V256C451,231.187,430.813,211,406,211z M136,150c0-66.168,53.832-120,120-120s120,53.832,120,120v61h-30
			v-61c0-49.626-40.374-90-90-90c-49.626,0-90,40.374-90,90v61h-30V150z M316,150v61H196v-61c0-33.084,26.916-60,60-60
			S316,116.916,316,150z M421,467c0,8.271-6.729,15-15,15H106c-8.271,0-15-6.729-15-15V256c0-8.271,6.729-15,15-15h300
			c8.271,0,15,6.729,15,15V467z"/></g></svg><span class="hidden">Correo</span></label>
                        <input id="regPassword" type="password" name="password" autocomplete="off" class="form__input" formenctype="application/x-www-form-urlencoded" placeholder="Contraseña" required>
                    </div>

                    <div class="form_field">
                        <label for="register_email"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 512 512" class="icon" xml:space="preserve"><g><path d="M298.885,372.957C309.746,361.839,316,346.795,316,331c0-33.084-26.916-60-60-60c-33.084,0-60,26.916-60,60
			c0,15.795,6.254,30.839,17.115,41.957l-16.575,60.052c-1.246,4.514-0.312,9.352,2.525,13.078S206.317,452,211,452h90
			c4.683,0,9.098-2.187,11.935-5.913c2.837-3.726,3.771-8.563,2.525-13.078L298.885,372.957z M267.442,371.793L281.299,422H230.7
			l13.857-50.207c1.684-6.101-0.643-12.603-5.816-16.25C230.764,349.918,226,340.743,226,331c0-16.542,13.458-30,30-30
			s30,13.458,30,30c0,9.743-4.764,18.918-12.742,24.543C268.085,359.191,265.758,365.692,267.442,371.793z"/></g><g><path d="M406,211v-61C406,67.29,338.71,0,256,0S106,67.29,106,150v61c-24.813,0-45,20.187-45,45v211c0,24.813,20.187,45,45,45h300
			c24.813,0,45-20.187,45-45V256C451,231.187,430.813,211,406,211z M136,150c0-66.168,53.832-120,120-120s120,53.832,120,120v61h-30
			v-61c0-49.626-40.374-90-90-90c-49.626,0-90,40.374-90,90v61h-30V150z M316,150v61H196v-61c0-33.084,26.916-60,60-60
			S316,116.916,316,150z M421,467c0,8.271-6.729,15-15,15H106c-8.271,0-15-6.729-15-15V256c0-8.271,6.729-15,15-15h300
			c8.271,0,15,6.729,15,15V467z"/></g></svg><span class="hidden">Correo</span></label>
                        <input id="regCPassword" type="password" name="consPassword" autocomplete="off" class="form__input" formenctype="application/x-www-form-urlencoded" placeholder="Confirmar contraseña" required>
                    </div>

                    <div class="form_field">
                        <input type="submit" value="Registrarse">
                    </div>

                    <p>Volver a la sección de <a id="goLog" href="#"><b>Ingreso</b></a></p>

                </form>
            </div>
        </div>
        `;

        var regLink = div.querySelector('#goReg');
        regLink.addEventListener('click', function showReg() {
            if ($(window).width() > 1200) {
                div.querySelector('.slideInit').style.left = 0;
            } else {
                //alert('Me falta asi');
            }
        });

        var backToLog = div.querySelector('#goLog');
        backToLog.addEventListener('click', function showLog() {
            if ($(window).width() > 1200 && div.querySelector('.slideInit').style.left == '0px') {
                div.querySelector('.slideInit').style.left = "33%";
            } else {
                console.log(div.querySelector('.slideInit').style.left);
            }
        });

        div.querySelector('#loginForm').addEventListener('submit', (e) => {
            e.preventDefault();
            if (e.target.username.value.includes('@')) {
                this.onLogin(e.target.username.value, e.target.password.value);
            } else {
                alert('Debe ser un correo valido');
            }
        });

        div.querySelector('#registerForm').addEventListener('submit', (e) => {
            e.preventDefault();
            if (e.target.password.value == e.target.consPassword.value) {
                this.onRegister(e.target.username.value, e.target.email.value, e.target.country.value, e.target.password.value);
            } else {
                alert('Las contraseñas deben de ser iguales');
            }
        });


        return div;
    },

    createHome: function createHome() {
        var div = document.createElement('div');
        div.setAttribute('id', 'home');

        div.innerHTML = `<header id="head">
                <div id="containerHeader">
                    <div id="logoNav">
                        <div id="separador"></div>
                    </div>
                    <select name="Discover" id="discover">
                    <option value="" disabled selected>Descubrir</option>
                    <option value="Mundial">Mundial</option>
                    <option value="America">America</option>
                    <option value="Europa">Europa</option>
                    <option value="Asia">Asia</option>
                    <option value="Africa">Africa</option>
                    <option value="Oceania">Oceania</option>
                    </select>
                    <form action="" class="search">
                        <div class="field">
                            <input type="text" class="input-search" id="input-search" required>
                            <label for="input-search">Buscar usuario...</label>
                        </div>
                    </form>
                    <div id="settings"></div>
                    <div id="notify"></div>
                    <div id="profile" style="background: url('${this.user.pic}') no-repeat; background-position: center;background-size: cover;"></div>
                    <div id="profileZone">
                        <input type="checkbox" id="toggleProfile" checked>
                        <div class="dropdown-profile">
                            <div class="arrow-right"></div>
                            <a href="#" id="changePic">Foto de perfil</a>
                            <a href="#" id="btnClose">Cerrar sesion</a>
                        </div>
                       
                    </div>
                </div>
            </header>
            <div id="posting">
                <input type="radio" id="tab1" name="tabs" class="tabs" checked>
                <label for="tab1">Escribir un post</label>

                <input type="radio" id="tab2" name="tabs" class="tabs">
                <label for="tab2">Subir una imagen</label>

                <input type="radio" id="tab3" name="tabs" class="tabs">
                <label for="tab3">Compartir ubicacion</label>

                <section id="posting1"><textarea name="post-to-send" id="post-to-send" placeholder="Expresate..." rows="3" required></textarea><button id="btnPost1">Publicar</button></section>
                <section id="posting2">
                    <div class="file-upload">
                        <div class="upload-content">
                            <img class="upload-image" src="#" alt="Imagen de post" />
                            <div class="title-wrap"><button type="button" class="remove-image">Eliminar <span class="img-title">imagen</span></button></div>
                        </div>
                        <div class="upload-wrap">
                            <input class="upload-input" type="file" name="picture" accept="image/*" />
                            <div class="drag-text"><h3>Arrastra tu imagen aqui</h3></div>
                        </div>
                    <button class="file-upload-btn" type="button" onclick="">Agregar imagen</button>
                    </div>
                    <textarea name="post-to-send-img" id="post-to-send-img" placeholder="Expresate..." rows="3" required></textarea>
                    <button id="btnPost2">Publicar</button>
                </section>
                <section id="posting3">
                    <p>Ubicacion...</p>
                </section>
            </div>
            <div id="allPost">
                <h2> Aun no hay posts :c </h2>
            </div>
            </div>`;

        var that = this;

        //Desplegar menu de perfil
        var profilePic = div.querySelector('#profile');
        profilePic.addEventListener('click', () => {
            $("#toggleProfile").trigger('click');
        });

        //Cerrar sesion
        var btnClose = div.querySelector('#btnClose');
        btnClose.addEventListener('click', (e) => {
            e.preventDefault();
            that.closeAccount();
        });

        var publicar = div.querySelector("#btnPost1");

        var allPost = div.querySelector("#allPost");

        //Crear post gurdados
        for (var i = 0; i < that.posted.length; i++) {
            if (i > 0) {
                allPost.insertBefore(that.createPost(that.posted[i].name, that.posted[i].country, that.posted[i].content, that.posted[i].likes, `../postImg/${that.posted[i].image}`, that.posted[i].comments, that.posted[i].profilePic, i), div.querySelectorAll('.post')[0]);
            } else {
                allPost.innerHTML = '';
                allPost.appendChild(that.createPost(that.posted[i].name, that.posted[i].country, that.posted[i].content, that.posted[i].likes, `../postImg/${that.posted[i].image}`, that.posted[i].comments, that.posted[i].profilePic, i));
            }
        }

        //Click al post de texto
        publicar.addEventListener('click', function (e) {
            e.preventDefault();

            if (div.querySelector('#post-to-send').value.length > 0) {
                that.onPost(that.user.name, that.user.country, div.querySelector('#post-to-send').value, [], "", [], that.user.pic, that.posted.length);

                div.querySelector('#post-to-send').value = "";
            } else {
                alert('El post debe contener texto')
            }
        });

        //Click al post de imagen
        var publicarImg = div.querySelector('#btnPost2');
        var inputImg = div.querySelector('.upload-input');
        publicarImg.addEventListener('click', (e) => {
            e.preventDefault();

            if (inputImg.files) {
                that.onPostImg(that.user.name, that.user.country, div.querySelector('#post-to-send').value, [], inputImg.files[0], [{name: "none"}], that.user.pic, that.posted.length);

                div.querySelector('#post-to-send').value = "";
            } else {
                alert('Agrega una imagen');
            }
        });

        //Borrar imagen
        var removeUpload = function () {
            $('.upload-input').val("");
            $('.upload-content').hide();
            $('.upload-wrap').show();
        };

        //Subir imagen
        var uploadBtn = div.querySelector(".file-upload-btn");
        uploadBtn.addEventListener('click', () => {
            $(".upload-input").trigger('click');
        });

        //Input obtiene imagen
        var uploadInput = div.querySelector(".upload-input");
        uploadInput.addEventListener('change', () => {

            var input = div.querySelector(".upload-input");;

            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = (e) => {
                    $('.upload-wrap').hide();
                    $('.upload-image').attr('src', e.target.result);
                    $('.upload-content').show();
                };

                reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        });

        var delUploadBtn = div.querySelector(".remove-image");
        delUploadBtn.addEventListener('click', () => {
            removeUpload();
        });

        $('.upload-wrap').bind('dragover', () => {
            $('.upload-wrap').addClass('dropped');
        });
        $('.upload-wrap').bind('dragleave', () => {
            $('.upload-wrap').removeClass('dropped');
        });

        return div;
    },

    createPost: function createPost(user, country, texto, liked, image, comentarios, profilePic, ident) {
        var post = document.createElement('div');
        post.setAttribute('class', 'post');

        postLikes = liked.split(',');
        var that = this;

        var that = this;
        if (image.length > 11) {
            post.innerHTML = `
            <div class="profilePost">
                    <div class="profilePicture" style="background: url('${profilePic}') no-repeat; background-position: center;background-size: cover;"></div>
                    <div class="profileInfo">
                        <h4>${user}</h4>
                        <p>${country}</p>
                    </div>
                    <p class="likes">${postLikes.length} Me gusta</p>
                    <div class="like" style="background: url('${(postLikes.indexOf(that.user.email) != -1) ? '../img/like_on.png' : '../img/like_off.png'}') no-repeat; background-position: center;background-size: contain;"></div>
                </div>
                <div class="contentPost">
                    <div class="imagePosted" style="background: url('${image}') no-repeat; background-position: center;background-size: cover;"></div>
                    <p class="textPosted">${texto}</p>
                </div>
                <div class="comentariosPost">
                </div>
                <div class="addComment">
                    <span>+</span>
                </div>
        `;
        } else {
            post.innerHTML = `
            <div class="profilePost">
                    <div class="profilePicture" style="background: url('${profilePic}') no-repeat; background-position: center;background-size: cover;"></div>
                    <div class="profileInfo">
                        <h4>${user}</h4>
                        <p>${country}</p>
                    </div>
                    <div class="addComment"></div>
                    <div class="like" style="background: url('${(postLikes.indexOf(that.user.email) != -1) ? '../img/like_on.png' : '../img/like_off.png'}') no-repeat; background-position: center;background-size: contain;"><p style="${(postLikes.indexOf(that.user.email) != -1) ? 'color: #fff' : 'color: #888'}">${postLikes.length}</p></div>
                </div>
                <div class="contentPost">
                    <div class="imagePosted" style="display: none;"></div>
                    <p class="textPosted">${texto}</p>
                </div>
                <div class="comentariosPost">
                </div>
        `;
        }

        console.log(postLikes.indexOf(that.user.email));
        var addCom = post.querySelector('.addComment');
        addCom.addEventListener('click',(e) => {
            var modal = that.createComment(ident,post.querySelector('.comentariosPost'));
            post.appendChild(modal);
            $('body').addClass('stop-scrolling')
        });

        var likes = post.querySelector('.like');
        var likeCount = likes.querySelector('p');

        likes.addEventListener('click', () => {
            var newLikes = postLikes;
            if (likes.style.background.includes('like_on')) {
                newLikes = postLikes.filter(function (e) {
                    return e !== that.user.email
                })
                likes.style.background = 'url("../img/like_off.png") no-repeat';
                likes.style.backgroundSize = 'contain';
                likes.style.backgroundPosition = 'center';
                likeCount.style.color = '#888';
            } else {
                newLikes.push(that.user.email);
                likes.style.background = 'url("../img/like_on.png") no-repeat';
                likes.style.backgroundSize = 'contain';
                likes.style.backgroundPosition = 'center';
                likeCount.style.color = '#fff';
            }

            likeCount.innerHTML = `${newLikes.length}`;
            postLikes = newLikes;
            that.likePost(that.user.email, ident, newLikes);
        });

        var coments = post.querySelector('.comentariosPost');

        for (var i = 1; i < comentarios.length; i++) {
            var com = document.createElement('div');
            com.setAttribute('class', 'comentario');

            com.innerHTML += `
                <div class="profilePicture" style="background: url('${comentarios[i].profilePic}') no-repeat;background-position: center;background-size: cover;"></div>
                <p><span class="commentName">${comentarios[i].name}</span><br>${comentarios[i].comment}</p>`;

            coments.insertBefore(com,coments.childNodes[0]);
        }

        return post;
    },
    
    createComment: function createComment (ident,comments) {
        var div = document.createElement('div');
        
        div.innerHTML = `
        <div class="modal-backdrop"></div>
        <div class="modal">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="btnCloseModal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Agregar un comentario</h4>
            </div>
            <div class="modal-body">
                <textarea name="comment-send" class="comment-send" placeholder="Escribe tu comentario..." rows="9" required></textarea>
                <button class="btnComment">Comentar</button>
            </div>
          </div>
        </div>
        `;
        
        var modal = div.querySelector('.modal');
        var backdrop = div.querySelector('.modal-backdrop');
        
        var that = this;
        
        var btnComment = div.querySelector('.btnComment');
        var commentContent = div.querySelector('.comment-send');
        btnComment.addEventListener('click', (e) => {
            e.preventDefault();
            var com = document.createElement('div');
            com.setAttribute('class', 'comentario');

            com.innerHTML += `
                <div class="profilePicture" style="background: url('${that.user.pic}') no-repeat;background-position: center;background-size: cover;"></div>
                <p><span class="commentName">${that.user.name}</span><br>${commentContent.value}</p>`;
            
            comments.insertBefore(com,comments.childNodes[0]);
            that.onComment(ident,commentContent.value,that.user.name,that.user.pic);
            remove();
        });
        
        modal.style.display = 'block';
        
        setTimeout(() => {
            modal.style.opacity = 1;
            backdrop.style.opacity = .6;
        });
        
        var remove = () => {
            modal.style.opacity = 0;
            backdrop.style.opacity = 0;
            if($('body').hasClass('stop-scrolling')){
                $('body').removeClass('stop-scrolling');
            }
            setTimeout(() => {
                div.remove();
            }, 500);
        };
        
        backdrop.addEventListener('click',(e) => {
            remove();
        });
        
        div.querySelector('.btnCloseModal').addEventListener('click',remove);
        
        return div;
    },

    render: function render(data, user) {

        var main = document.querySelector('#main');
        var init = this.createInit();
        var home = this.createHome();

        main.innerHTML = '';
        if (this.user.email == "") {
            main.appendChild(init);
        } else {
            main.appendChild(home);
        }
    }
}
