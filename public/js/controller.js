var controller = function controller(view) {
    var info = this.model;

    if (localStorage.getItem('name')) {
        view.user.name = localStorage.getItem('name');
        view.user.email = localStorage.getItem('email');
        view.user.country = localStorage.getItem('country');
        //view.user.pic = localStorage.getItem('pic');
        console.log('El usuario esta logeado');
    } else {
        console.log('El usuario no esta logeado')
    }

    view.onRegister = (username, email, country, password) => {
        var params = new URLSearchParams();
        params.set('name', username);
        params.set('email', email);
        params.set('country', country);
        params.set('password', password);

        fetch(`${location.origin}/api/crearUsuario`, {
                method: 'POST',
                body: params
            })
            .then((res) => res.json())
            .then((res) => console.log(res));
    }

    view.onLogin = (email, password) => {
        var params = new URLSearchParams();
        params.set('email', email);
        params.set('password', password);

        fetch(`${location.origin}/api/login`, {
                method: 'POST',
                body: params
            })
            .then((res) => res.json())
            .then((res) => {
                if (res.msg == 'logged') {
                    console.log(res);
                    view.user = res.user;
                    localStorage.setItem('name', view.user.name);
                    localStorage.setItem('email', view.user.email);
                    localStorage.setItem('country', view.user.country);
                    localStorage.setItem('pic', view.user.pic);
                    console.log(localStorage.getItem('name'));
                    view.render();
                }
            });
    }

    view.closeAccount = () => {
        view.user.name = "";
        view.user.pass = "";
        view.user.email = "";
        view.user.country = "";
        view.user.pic = "";
        localStorage.removeItem('name');
        localStorage.removeItem('email');
        localStorage.removeItem('country');
        localStorage.removeItem('pic');
        view.render();
        console.log('Sesion cerrada');
    }

    view.onPost = (name, country, content, likes, image, comments, profilePic, ident) => {
        var params = new URLSearchParams();
        params.set('name', name);
        params.set('country', country);
        params.set('content', content);
        params.set('likes', likes);
        params.set('image', image);
        params.set('comments', comments);
        params.set('profilePic', profilePic);
        params.set('ident', ident);
        console.log('Post texto');
        fetch(`${location.origin}/api/crearPost`, {
                method: 'POST',
                body: params
            })
            .then((res) => res.json())
            .then((res) => {
                if (res.msg == 'posted') {
                    console.log(res);
                    view.posted.push(res.post);
                    view.render();
                }
            });
    }

    view.onPostImg = (name, country, content, likes, image, comments, profilePic, ident) => {
        var params = new FormData();
        params.set('name', name);
        params.set('country', country);
        params.set('content', content);
        params.set('likes', likes);
        params.set('image', image);
        params.set('comments', comments);
        params.set('profilePic', profilePic);
        params.set('ident', ident);
        console.log('Post imagen');
        fetch(`${location.origin}/api/crearPostImg`, {
                method: 'POST',
                body: params
            })
            .then((res) => res.json())
            .then((res) => {
                if (res.msg == 'posted') {
                    console.log(res);
                    view.posted.push(res.post);
                    view.render();
                } else {
                    console.log(res);
                }
            });
    }

    view.likePost = (email, ident, allLikes) => {
        var params = new URLSearchParams();
        params.set('email', email);
        params.set('ident', ident);
        params.set('allLikes', allLikes);
        console.log('Le di like');
        fetch(`${location.origin}/api/likePost`, {
                method: 'POST',
                body: params
            })
            .then((res) => res.json())
            .then((res) => {
                if (res.msg == 'liked') {
                    console.log(res);
                }
            });
    }

    view.onComment = (ident, content, name, profilePic) => {
        var params = new URLSearchParams();
        params.set('ident', ident);
        params.set('content', content);
        params.set('name', name);
        params.set('profilePic', profilePic);
        console.log('Comente '+ident);
        fetch(`${location.origin}/api/commentPost`, {
                method: 'POST',
                body: params
            })
            .then((res) => res.json())
            .then((res) => {
                if (res.msg == 'commented') {
                    console.log(res);
                }
            });
    }

    console.log(location.origin);
    fetch(`${location.origin}/api/posts`)
        .then((res) => res.json())
        .then((res) => {
            if (res.msg == 'posts loaded') {
                console.log(res.msg);
                view.posted = res.posts;
                view.render();
            }
        });
}
controller(view);
